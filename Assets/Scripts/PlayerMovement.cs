using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.InputSystem;
using UnityEngine.Scripting.APIUpdating;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float mouseSensitivity = 10f;
    [SerializeField] Vector2 verticalLookLimit = new Vector2(-10f, 10f);
    [SerializeField] Vector2 horizontalLookLimit = new Vector2(-10f, 10f);
    [SerializeField] float walkSpeed = 10f;

    Vector2 lookInput;
    Vector2 moveInput;
    Rigidbody rb;

    Game game;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        lookInput = new Vector2(transform.rotation.y, transform.rotation.x);
        game = GameObject.Find("Player").GetComponent<Game>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (game.IsPaused) return;
        Look();
        Move();
    }

    private void Look()
    {
        transform.rotation = Quaternion.Euler(lookInput.y * mouseSensitivity, lookInput.x * mouseSensitivity, 0);
    }

    private void Move()
    {
        rb.velocity = new Vector2(moveInput.x * walkSpeed, rb.velocity.y);
    }

    void OnLook(InputValue value) {
        if (game.IsPaused) return;
        Vector2 updatedValue = value.Get<Vector2>() * mouseSensitivity * new Vector2(1, -1) * Time.deltaTime;
        lookInput += updatedValue;
        if (lookInput.y > verticalLookLimit.y) lookInput.y = verticalLookLimit.y;
        if (lookInput.y < verticalLookLimit.x) lookInput.y = verticalLookLimit.x;
        if (lookInput.x > horizontalLookLimit.y) lookInput.x = horizontalLookLimit.y;
        if (lookInput.x < horizontalLookLimit.x) lookInput.x = horizontalLookLimit.x;
    }
  
    void OnMove(InputValue value) {
        if (game.IsPaused) return;
        moveInput = value.Get<Vector2>();
    }
  
}
