using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Phone : MonoBehaviour
{
    [SerializeField] GameObject jokeTopic;
    [SerializeField] float initialPosition = 20f;
    [SerializeField] float jokeSpacing = 10f;
    [SerializeField] float hideDistance = 0.5f;
    [SerializeField] float hideSpeed = 0.5f;

    Game game;
    GameObject[] jokeObjects = new GameObject[0];
    Game.JokeType[] localJokesPool = new Game.JokeType[0];

    Vector2 verticalPositionLimit = new Vector2(0, 0);

    // Start is called before the first frame update
    void Awake()
    {
        game = GameObject.Find("Player").GetComponent<Game>();
        verticalPositionLimit = new Vector2(transform.localPosition.y, transform.localPosition.y - hideDistance);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (game.IsPaused) return;
        UpdatePosition();
        UpdateJokes();
    }

    private void UpdatePosition()
    {
        if (game.phoneDirection == 0) return;
        float newVerticalPosition = transform.localPosition.y + game.phoneDirection * hideSpeed * Time.deltaTime;
        if (newVerticalPosition >= verticalPositionLimit.x || newVerticalPosition <= verticalPositionLimit.y) {
            game.phoneDirection = 0;
            newVerticalPosition = transform.localPosition.y;
        }
        transform.localPosition = new Vector3(transform.localPosition.x, newVerticalPosition, transform.localPosition.z);
    }

    private void UpdateJokes()
    {
        if (Enumerable.SequenceEqual(game.jokesPool, localJokesPool)) return;
        localJokesPool = new Game.JokeType[game.jokesPool.Length];
        Array.Copy(game.jokesPool, localJokesPool, game.jokesPool.Length);
        ClearJokes();
        jokeObjects = new GameObject[localJokesPool.Length];
        for (int i = 0; i < localJokesPool.Length; i++) {
            Game.JokeType jokeType = localJokesPool[i];
            if (jokeType == Game.JokeType.JOKE_TYPE_NONE) continue;
            jokeObjects[i] = GetJoke(i, Game.jokesText[localJokesPool[i]]);
        }
    }

    GameObject GetJoke(int position, string text = "") {
        GameObject joke = Instantiate(jokeTopic, Vector3.zero, Quaternion.identity, transform.Find("Canvas"));
        RectTransform rt = joke.GetComponent<RectTransform>();
        rt.anchoredPosition3D = new Vector3(0, initialPosition - position * jokeSpacing, 0);
        rt.localRotation = Quaternion.Euler(Vector3.zero);
        joke.transform.Find("Text").GetComponent<TMPro.TextMeshProUGUI>().text = $"{position + 1}. {text}";
        return joke;
    }

    void ClearJokes() {
        foreach (GameObject jokeObject in jokeObjects) {
            Destroy(jokeObject);
        }
    }
}
