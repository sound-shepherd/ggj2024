using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spectator : MonoBehaviour
{
    [HideInInspector]
    float mood;
    float boredom = 1.5f;
    [HideInInspector]
    public float[] jokeTolerance;
    float hitDamage = 15.0f;
    float hitJoy = 0.75f;
    public bool isAlive;
    [HideInInspector]
    public BoxCollider collider;
    float[] normiesRange = { 1, 3 };
    float loyalty, loyaltyLossCoef = 3f;
    float loyaltyCoefficient = 0.2f;
    SpriteRenderer Indicator;
    public Sprite IndicatorSprite;
    private float offset = 1.25f;
    private float maxMood = 20f;
    public AudioClip hitSound1 = null;
    public AudioClip hitSound2 = null;
    void Start()
    {
        mood = 0;
        jokeTolerance = new float[(int)Game.JokeType.JOKE_TYPE_MAX];
        GenerateJokeTolerance();
        isAlive = true;
        collider = GetComponent<BoxCollider>();
        GameObject newObject = new GameObject("NewSpriteRenderer");
        Indicator = newObject.AddComponent<SpriteRenderer>();
        Indicator.sprite = IndicatorSprite;
        //Indicator.color = Color.green;
        Indicator.transform.localScale = new Vector3(3, 3, 3);
        newObject.transform.SetParent(transform);
        Indicator.transform.position = new Vector3(transform.position.x, transform.position.y + offset, transform.position.z);
        HideMood();
    }
    public float MoodPercent()
    {
        return (mood / maxMood);
    }
    public void IndicateMood()
    {
        float g = 0.5f + MoodPercent();
        float r = 0.5f - MoodPercent();
        //Indicator.color = Color.red * r + Color.green * g;

        var tp = GetComponent<TexturePicker>();

        if (mood < -0.2 * maxMood)
        {
            tp.SetMood(TexturePicker.Mood.Angry);
        }
        else if (mood > 0.5 * maxMood)
        {
            tp.SetMood(TexturePicker.Mood.Happy);
        }
        else
        {
            tp.SetMood(TexturePicker.Mood.Neutral);
        }
    }
    public void HideMood()
    {
        Indicator.color = new Color();
    }
    float GenerateApproximateNormalDistributionInRange(float min, float max, float standardDeviation)
    {
        float mean = (max + min) / 2;
        float randomValue = Random.Range(-1f, 1f);
        float scaledValue = mean + standardDeviation * randomValue;
        return Mathf.Clamp(scaledValue, min, max);
    }
    void GenerateJokeTolerance()
    {
        loyalty = 0;

        for(int i = 0; i < (int)Game.JokeType.JOKE_TYPE_MAX; i++)
        {
            jokeTolerance[i] = GenerateApproximateNormalDistributionInRange(normiesRange[0] - Game.jokesVolatility[i], normiesRange[1] + Game.jokesVolatility[i], Game.jokesVolatility[i]);
        }
    }
    public void Revive()
    {
        GetComponent<WalkAnimation>().Enter();
        Start();
    }
    public void ChangeMood(float delta) //constant change
    {
        mood = Mathf.Clamp(mood + delta, -maxMood, maxMood);
        if (mood <= -maxMood)
        {
            GetComponent<WalkAnimation>().Leave();
            isAlive = false;
        }
        IndicateMood();
    }
    public float TakeJoke(Game.JokeType jokeType)
    {
        float jokePower = Random.Range(0.5f, 2f);
        float delta = ((jokeTolerance[(int)jokeType] < 0) ? (1 / jokePower) : jokePower) * jokeTolerance[(int)jokeType] + loyalty - boredom;
        mood = Mathf.Clamp(mood + delta, -maxMood, maxMood);
        if (mood <= -maxMood)
        {
            GetComponent<WalkAnimation>().Leave();
            isAlive = false;
        }
        loyalty /= loyaltyLossCoef;
        loyalty += delta * loyaltyCoefficient;
        IndicateMood();
        return delta;
    }
    public void TakeHit()
    {
        if(Random.Range(0.0f, 1.0f) < 0.5f)
        {
            GetComponent<AudioSource>().PlayOneShot(hitSound1);
        }
        else
        {
            GetComponent<AudioSource>().PlayOneShot(hitSound2);
        }
        ChangeMood(-hitDamage);
    }
    public void SeeHit()
    {
        ChangeMood(+hitJoy);
    }
}
