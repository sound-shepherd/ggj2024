using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class TexturePicker : MonoBehaviour
{
    public enum Mood
    {
        Neutral,
        Angry,
        Happy,
    }

    public Texture2D[] bodies;
    public Texture2D[] neutralFaces;
    public Texture2D[] angryFaces;
    public Texture2D[] happyFaces;

    public MeshRenderer myRenderer;

    private Material myHeadMat;
    private Mood myMood = Mood.Neutral;

    private int bodyIndex = -1;
    private int headIndex = -1;

    void Start()
    {
        Assert.AreEqual(neutralFaces.Length, angryFaces.Length);
        Assert.AreEqual(neutralFaces.Length, happyFaces.Length);

        bodyIndex = UnityEngine.Random.Range(0, bodies.Length);
        headIndex = UnityEngine.Random.Range(0, neutralFaces.Length);

        var mats = myRenderer.materials;
        Assert.AreEqual(mats.Length, 2);

        mats[0].SetTexture("_MainTex", bodies[bodyIndex]);
        mats[1].SetTexture("_MainTex", neutralFaces[headIndex]);

        myHeadMat = mats[1];
        myMood = Mood.Neutral;
    }

    public void SetMood(Mood mood)
    {
        if (mood == myMood)
        {
            return;
        }

        myMood = mood;

        switch (mood)
        {
            case Mood.Neutral:
                myHeadMat.SetTexture("_MainTex", neutralFaces[headIndex]); break;
            case Mood.Angry:
                myHeadMat.SetTexture("_MainTex", angryFaces[headIndex]); break;
            case Mood.Happy:
                myHeadMat.SetTexture("_MainTex", happyFaces[headIndex]); break;
        }
    }
}
