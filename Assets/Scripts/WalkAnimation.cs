using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkAnimation : MonoBehaviour
{
    public enum State
    {
        // Staying in place
        Idle,
        // Going towards its place
        Entering,
        // Going away from its place
        Leaving,
        // Left the room
        Left
    }

    private State state = State.Idle;
    private float walkTimeLeft = 0.0f;
    private float walkTimeFull = 0.0f;
    private Vector3 originalPlace;
    private Vector3 sourcePlace;
    private Vector3 targetPlace;
    private bool exitIsRight;

    public float walkSpeed = 1.5f;
    public float walkBobFreq = 15.0f;
    public float walkBobAmp = 0.1f;
    public float walkLeaveThreshold = 9.0f;

    void Start()
    {
        originalPlace = transform.position;
        exitIsRight = originalPlace.x > 0.0f;
    }

    void Update()
    {
        if (state == State.Idle || state == State.Left)
        {
            return;
        }

        walkTimeLeft -= Time.deltaTime;
        if (walkTimeLeft < 0.0f)
        {
            walkTimeLeft = 0.0f;
            transform.position = targetPlace;

            if (state == State.Entering)
            {
                state = State.Idle;
            }
            else
            {
                state = State.Left;
            }

            return;
        }

        float x = Mathf.Lerp(targetPlace.x, sourcePlace.x, walkTimeLeft / walkTimeFull);
        float dy = Mathf.Sin(walkTimeLeft * walkBobFreq) * walkBobAmp;
        transform.position = new Vector3(x, originalPlace.y + dy, originalPlace.z);
    }

    public void Leave()
    {
        if (state != State.Leaving)
        {
            sourcePlace = transform.position;
            float exitTarget = exitIsRight ? walkLeaveThreshold : -walkLeaveThreshold;
            targetPlace = originalPlace + new Vector3(exitTarget, 0.0f, 0.0f);

            walkTimeFull = Mathf.Abs(originalPlace.x - exitTarget) / walkSpeed;
            walkTimeLeft = walkTimeFull;
        }

        state = State.Leaving;
    }

    public void Enter()
    {
        if (state != State.Entering)
        {
            sourcePlace = transform.position;
            targetPlace = originalPlace;

            walkTimeFull = Mathf.Abs(originalPlace.x - transform.position.x) / walkSpeed;
            walkTimeLeft = walkTimeFull;
        }

        state = State.Entering;
    }

    public State GetState() { return state; }
}
