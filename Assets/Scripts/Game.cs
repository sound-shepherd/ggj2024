using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Video;
using System;
using TMPro;
using Unity.VisualScripting;

public class Game : MonoBehaviour
{
    public enum State
    {
        STATE_INTRO = 0,
        STATE_MENU,
        STATE_STARTED,
        STATE_PAUSED,
        STATE_SCORE
    }
    enum GameplayState
    {
        INGAME_STATE_JOKE_AVAILABLE = 0,
        INGAME_STATE_JOKE_REACTION
    }
    public enum JokeType : int
    {
        JOKE_TYPE_NONE = -1,
        JOKE_TYPE_1,
        JOKE_TYPE_2,
        JOKE_TYPE_3,
        JOKE_TYPE_4,
        JOKE_TYPE_5,
        JOKE_TYPE_6,
        JOKE_TYPE_7,
        JOKE_TYPE_8,
        JOKE_TYPE_MAX
    }

    public static Dictionary<JokeType, string> jokesText = new Dictionary<JokeType, string>()
    {
        { JokeType.JOKE_TYPE_1, "\uf004" },
        { JokeType.JOKE_TYPE_2, "\uf0f5" },
        { JokeType.JOKE_TYPE_3, "\uf29b" },
        { JokeType.JOKE_TYPE_4, "\uf2cc" },
        { JokeType.JOKE_TYPE_5, "\uf2be" },
        { JokeType.JOKE_TYPE_6, "\uf13d" },
        { JokeType.JOKE_TYPE_7, "\uf0e7" },
        { JokeType.JOKE_TYPE_8, "\uf207" }
    };

    public static float[] jokesVolatility = //LOOK UP FOR JokeType
    {
        2,
        2,
        2,
        2,
        5,
        5,
        5,
        5
    };

    public State gameState;
    GameplayState gameplayState;
    public static int JOKES_AMOUNT_IN_STACK = 4;
    public JokeType[] jokesPool;
    public static Spectator[] spectators;
    public bool IsJokesReady;
    public GameObject projectile;
    public float shootForceForward = 20.0f;
    public float shootForceUp = 3.5f;
    public float shootForceRandom = 1.5f;
    public int phoneDirection = 0;
    public float phoneShowDelay = 1f;
    public float jokeSayingDelay = 2f;
    public float audienceReactionDelay = 1f;
    private float angerTimer = 3f, angerTimerMax = 3f, angerImpact = 2f;
    private float boredomImpact = 0.5f;
    private float GameScore = 0f;
    private float angryThreshold = -1.5f, happyThreshold = 1.5f;
    int jokesAmount = 10, jokesMaximum = 10;
    bool isDogBusy = false;
    public AudioSource audience; 
    public AudioClip[] happySound; 
    public AudioClip[] angrySound; 
    public AudioClip[] boringSound; 
    public AudioClip[] jokeSound; 

    public VideoPlayer introVideo;
    public AudioSource ambienceSound;

    public bool IsPaused {
        get {
            return gameState != State.STATE_STARTED;
        }
    }

    public int AliveSpectators {
        get {
            return Array.FindAll(spectators, s => s.isAlive).Length;
        }
    }

    GameObject mainMenu;
    GameObject pauseMenu;
    GameObject player;
    GameObject result;

    void Start()
    {
        gameState = State.STATE_INTRO;
        gameplayState = GameplayState.INGAME_STATE_JOKE_AVAILABLE;
        IsJokesReady = false;
        jokesPool = new JokeType[JOKES_AMOUNT_IN_STACK];
        Array.Fill(jokesPool, JokeType.JOKE_TYPE_NONE);
        spectators = FindObjectsByType<Spectator>(FindObjectsSortMode.None);
        mainMenu = GameObject.Find("Main Menu");
        mainMenu.SetActive(false);
        pauseMenu = GameObject.Find("Pause Menu");
        pauseMenu.SetActive(false);
        player = GameObject.Find("HUD");
        player.SetActive(false);
        result = GameObject.Find("Result");
        result.SetActive(false);
        ambienceSound.Pause();
        InvokeRepeating("CheckIntroVideo", 10.0f, 0.5f);
    }

    public void MakeJokes()
    {
        for (int i = 0; i < JOKES_AMOUNT_IN_STACK; i++)
        {
            JokeType generatedJoke = (JokeType)UnityEngine.Random.Range((int)JokeType.JOKE_TYPE_1, (int)JokeType.JOKE_TYPE_8);
            while (Array.Exists(jokesPool, p => p == generatedJoke))
            {
                generatedJoke = (JokeType)UnityEngine.Random.Range((int)JokeType.JOKE_TYPE_1, (int)JokeType.JOKE_TYPE_8);
            }
            jokesPool[i] = generatedJoke;
        }
    }
    void throwJoke(int index)
    {
        int happyCount = 0, angryCount = 0;
        foreach (Spectator s in spectators)
        {
            if (s.isAlive)
            {
                float delta = s.TakeJoke(jokesPool[index]);
                s.IndicateMood();
                GameScore += s.MoodPercent();
                angryCount += (angryThreshold > delta ? 1 : 0);
                happyCount += (happyThreshold < delta ? 1 : 0);
            }
        }
        if (angryCount < 6 && happyCount < 6)
        {
            audience.PlayOneShot(boringSound[UnityEngine.Random.Range(0, boringSound.Length)]);
        }
        if (happyCount > 6)
        {
            audience.PlayOneShot(happySound[UnityEngine.Random.Range(0, happySound.Length)], (float)happyCount / spectators.Length);
        }
        if (angryCount > 6)
        {
            audience.PlayOneShot(angrySound[UnityEngine.Random.Range(0, angrySound.Length)], (float)(angryCount + 5) / spectators.Length);
        }
        if(--jokesAmount == 0)
        {
            jokesAmount = jokesMaximum;
            gameState = State.STATE_SCORE;
        }
    }

    void Update()
    {
        switch(gameState)
        {
            case State.STATE_INTRO:
            {
                break;
            }
            case State.STATE_MENU:
            {
                break;
            }
            case State.STATE_STARTED:
            {
                if (AliveSpectators == 0) {
                    gameState = State.STATE_SCORE;
                    break;
                }
                switch(gameplayState)
                {
                    case GameplayState.INGAME_STATE_JOKE_AVAILABLE:
                    {
                        if(!IsJokesReady)
                        {
                            MakeJokes();
                            IsJokesReady = true;
                        }
                        isDogBusy = false;
                        break;
                    }
                    case GameplayState.INGAME_STATE_JOKE_REACTION:
                    {
                        jokesPool = new JokeType[JOKES_AMOUNT_IN_STACK];
                        Array.Fill(jokesPool, JokeType.JOKE_TYPE_NONE);
                        IsJokesReady = false;
                        StartCoroutine(WaitForReaction());
                        break;
                    }
                }
                break;
            }
            case State.STATE_PAUSED:
            {
                break;
            }
            case State.STATE_SCORE:
            {
                result.SetActive(true);
                var angry = result.transform.Find("Angry").gameObject;
                var good = result.transform.Find("Good").gameObject;
                var neutral = result.transform.Find("Neutral").gameObject;
                angry.SetActive(false);
                good.SetActive(false);
                neutral.SetActive(false);
                if (AliveSpectators > spectators.Length * 0.6) {
                    good.SetActive(true);
                } else if (AliveSpectators < spectators.Length * 0.3) {
                    angry.SetActive(true);
                } else {
                    neutral.SetActive(true);
                }
                result.transform.Find("Score").GetComponent<TextMeshProUGUI>().text = "Your Score: " + ((int)Mathf.Max(0, GameScore)).ToString();
                break;
            }
        }
    }

    IEnumerator WaitForReaction()
    {
        yield return new WaitForSecondsRealtime(phoneShowDelay);
        phoneDirection = 1;
        gameplayState = GameplayState.INGAME_STATE_JOKE_AVAILABLE;
        StopCoroutine(WaitForReaction());
    }
    IEnumerator SwitchToReaction(int jokeIndex)
    {
        audience.PlayOneShot(jokeSound[UnityEngine.Random.Range(0, jokeSound.Length)], 0.5f);
        yield return new WaitForSecondsRealtime(jokeSayingDelay);
        throwJoke(jokeIndex);
        gameplayState = GameplayState.INGAME_STATE_JOKE_REACTION;
        StopCoroutine(SwitchToReaction(jokeIndex));
    }

    void OnChooseJoke(InputValue value)
    {
        if (!IsPaused && !isDogBusy)
        {
            isDogBusy = true;
            Vector2 val = value.Get<Vector2>();
            int jokeIndex = 0;
            if (val.Equals(Vector2.down))
            {
                jokeIndex = 1;
            }
            else if (val.Equals(Vector2.left))
            {
                jokeIndex = 2;
            }
            else if (val.Equals(Vector2.right))
            {
                jokeIndex = 3;
            }
            phoneDirection = -1;
            StartCoroutine(SwitchToReaction(jokeIndex));
        } 
    }

    void OnFire(InputValue value)
    {
        if (IsPaused || !value.isPressed)
        {
            return;
        }

        var offset = transform.forward * 0.3f + transform.right * 2.0f + transform.up * 0.85f;
        var force = transform.forward * shootForceForward + transform.up * shootForceUp + UnityEngine.Random.insideUnitSphere * shootForceRandom;

        GameObject proj = Instantiate(projectile, transform.position + offset, transform.rotation);
        proj.GetComponent<Rigidbody>().velocity = force;
    }
    
    public void CheckIntroVideo()
    {
        if (introVideo.frame >= Convert.ToInt64(introVideo.frameCount) - 5)
        {
            StopVideo();
        }
    }

    void StopVideo()
    {
        if (gameState != State.STATE_INTRO) return;
        introVideo.Stop();
        ambienceSound.Play();
        mainMenu.SetActive(true);
        CancelInvoke("CheckIntroVideo");
        gameState = State.STATE_MENU;
    }

    public void StartGame()
    {
        if (gameState == State.STATE_MENU || gameState == State.STATE_PAUSED || gameState == State.STATE_SCORE)
        {
            if (gameState != State.STATE_MENU) {
                foreach(Spectator s in spectators)
                {
                    s.Revive();
                }
            }
            gameState = State.STATE_STARTED;
            mainMenu.SetActive(value: false);
            player.SetActive(true);
            result.SetActive(false);
            pauseMenu.SetActive(false);
            GameScore = 0f;
        }
    }
    
    public void ExitGame()
    {
        Application.Quit();
    }
    
    public void ResumeGame()
    {
        if (gameState == State.STATE_PAUSED)
        {
            gameState = State.STATE_STARTED;
            pauseMenu.SetActive(false);
        }
    }

    void OnPause(InputValue value)
    {
        if (value.isPressed)
        {
            if (gameState == State.STATE_INTRO) {
                StopVideo();
                return;
            }
            if (!IsPaused)
            {
                gameState = State.STATE_PAUSED;
                pauseMenu.SetActive(true);
            } else {
                ResumeGame();
            }
        }
    }
    void SpreadBoredom()
    {
        foreach (Spectator s in spectators)
        {
            s.ChangeMood(-Mathf.Abs(s.MoodPercent()) * boredomImpact);
        }
    }
    void SpreadAnger()
    {
        foreach(Spectator spec in spectators)
        {
            if (spec.isAlive)
            {
                if (spec.MoodPercent() < -0.5f)
                {
                    if (UnityEngine.Random.Range(-1, 0.5f) > spec.MoodPercent())
                    {
                        float angerAmount = spec.MoodPercent();
                        foreach (Spectator s in spectators)
                        {
                            s.ChangeMood(angerImpact * angerAmount);
                        }
                    }
                }
            }
        }
    }
    void FixedUpdate()
    {
        if (IsPaused) return;
        if (angerTimer > 0f)
        {
            angerTimer -= Time.deltaTime;
        }
        else
        {
            angerTimer = angerTimerMax;
            SpreadAnger();
            SpreadBoredom();
        }
    }
}
